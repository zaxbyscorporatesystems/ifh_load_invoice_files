﻿using System;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using WinSCP;


namespace IFH_Load_Invoice_Files
{
    public partial class Form1 : Form
    {
        string _LocalDirectory = @"\\goober\ftpusers\external\VendorInvoices\IFH\In\",     //Local directory where the files will be downloaded
    _logFyle = string.Empty;

       // DateTime dteLookFor1 = DateTime.Now;//.AddDays(-1);
        string currentdate = string.Empty;
        string currenttime = string.Empty;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           // _logFyle = _LocalDirectory + "//PFG Load.log";

            // IFH import
            string _ftpURL = "ftp3.pfgc.com",     //"ftp://ftp3.pfgc.com",             //Host URL or address of the FTP server
                _UserName = "zaxby00",                 //User Name of the FTP server
                _Password = "zaX$qGQ#",              //Password of the FTP server
                _ftpDirectory = "outbound",          //The directory in FTP server where the files are present
                _FileName = string.Empty;             //File name, which one will be downloaded
            DateTime dteLookFor = DateTime.Now;     //.AddDays(-3);
            if(dteLookFor.DayOfWeek.ToString().ToUpper()=="SUNDAY")
            {
                dteLookFor = dteLookFor.AddDays(-1);
            }
            currentdate = dteLookFor.ToString("yyyy") + dteLookFor.ToString("MM") + dteLookFor.ToString("dd");
            currenttime = dteLookFor.ToString("hh") + dteLookFor.ToString("mm") + dteLookFor.ToString("ss");
            // File.AppendAllText(_logFyle, DateTime.Now + "---Process start" + Environment.NewLine);
            _FileName = "IFH_" + currentdate + "*.txt";             //File name, which one will be downloaded
            try
            {
                // Setup session options
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Ftp,        // .Sftp,
                    HostName = _ftpURL,
                    UserName = _UserName,
                    Password = _Password
                };

                using (Session session = new Session())
                {
                    session.ExecutablePath = @"C:\Program Files\WinSCP\WinSCP.exe";
                    // Connect
                    session.Open(sessionOptions);

                    // Download files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;

                    TransferOperationResult transferResult;
                    transferResult = session.GetFiles($@"/{_ftpDirectory}/{_FileName}", $@"{_LocalDirectory}{_FileName}", false, transferOptions);

                    // Throw on any error
                    transferResult.Check();

                    // Print results
                    foreach (TransferEventArgs transfer in transferResult.Transfers)
                    {
             //           Console.WriteLine("Download of {0} succeeded", transfer.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
               Console.WriteLine("Error: {0}", ex.Message);
            }
            Application.Exit();
        }

    }
}
